# Kuliah Web Programming

## _Intro JavaScript_

Kuliah hari ini merupakan kuliah mandiri untuk pengenalan JavaScript. Untuk memulai kuliah ini ikuti langkah-langkah dibawah ini:

### Menjalankan Program

1. Clone project ini kedalam folder document root web server anda
2. Jalankan webserver anda (xampp/laragon)
3. Buka project ini di browser anda
4. Setelah muncul tampilan web, Klik tombol **Vote** kemudian amati yang terjadi.
5. Jika kurang yakin, buka **Inspect Element** dan buka tab **Console**. Kemudian klik-klik kembali tombol **Vote** sampai anda menjadi yakin.

Jika anda sudah selesai melakukan pengamatan pada program tersebut, silahkan buka source code menggunakan aplikasi text editor kesayangan anda. Kemudian jawab pertanyaan ini di dalam hati anda:

1. Sebutkan struktur folder & file pada project!
2. File JavaScript berada di folder mana?
3. Bagaimana cara memanggil file JavaScript ke file HTML?
4. Coba berikan _comment_ di setiap baris kodingan JavaScript!
5. Perhatikan fungsi vote pada kodingan JavaScript!
6. Apa maksud dari _function vote(id)_?
7. Berapa ukuran array score?
8. Apa nilai awal array score?
9. Bagaimana cara memanggil fungsi **vote** di HTML?
10. Apa yang dimaksud **vote(0)**, **vote(1)**, **vote(2)**?
11. Apa yang dimaksud dengan _getElementById_ ?
12. Apa yang dimaksud dengan _innerHTML_ ?
13. Apa fungsi dari _console.log_ ?

Jawaban dari soal tersebut tidak dikumpulkan. Namun dipahami dalam hati & pikiran. Jika masih belum bisa memahami silahkan diulang-ulang dalam menjawab pertanyaan di atas.

## Tugas yang harus dikerjakan

Silahkan memodifikasi program tersebut. Tambahkan fitur yang dapat menampilkan prosentase perolehan suara tiap-tiap kandidat dan Total keseluruhan suara/voting yang sudah masuk ke sistem. Dipresentasikan di pertemuan berikutnya!
