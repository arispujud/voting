var score = [0, 0, 0];
var total = 0;
function vote(id) {
  score[id] += 1;
  total += 1;

  const element1 = document.getElementById("score-1");
  const element2 = document.getElementById("score-2");
  const element3 = document.getElementById("score-3");
  const elementTotal = document.getElementById("total");
  const persen1 = document.getElementById("score-p-1");
  const persen2 = document.getElementById("score-p-2");
  const persen3 = document.getElementById("score-p-3");

  element1.innerHTML = score[0];
  element2.innerHTML = score[1];
  element3.innerHTML = score[2];
  elementTotal.innerHTML = total;

  persen1.innerHTML = (score[0] / total) * 100;
  persen2.innerHTML = (score[1] / total) * 100;
  persen3.innerHTML = (score[2] / total) * 100;

  console.log(score);
}
